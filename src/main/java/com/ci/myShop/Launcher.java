package com.ci.myShop;

import java.util.ArrayList;
import java.util.List;
import com.ci.myShop.controller.Shop;
import com.ci.myShop.model.Item;

public class Launcher {
	
	//Declaration des variables Items et Shop
	private static List<Item> items = new ArrayList<Item>();
	private static Shop shop= null;
	
	public static void main(String[] args) {

	//Rajouter des items dans une liste
		items.add(new Item("Zelda", 1, 200, 2));
		items.add(new Item("Zelda: 2", 2, 15, 3));
		items.add(new Item("Zelda: A Link to the Past", 3, 100, 5));
		items.add(new Item("Zelda: Ocarina of Time", 4, 30, 5));
		items.add(new Item("Zelda: Skyward Sword", 5, 20, 15));
	
	//ajouter deux items 
		Item item1 = new Item("Zelda: Breath of The Wild", 6, 30, 1);
		Item item2 = new Item("Zelda: Awakening", 7, 20, 1);
    	
	//ajouter un item à la liste
		items.add(item1);
    	
	//Creer une shop initialisé avec la liste
		shop = new Shop(items);
		
	//Vendre un element de la liste
		shop.sell("Zelda: Skyward Sword");
		
	//mettre à jour le cash
		float resultat = shop.updateCash();
		
	//Imprimer le cash
		System.out.println(resultat);
		
    //execute la fonction buy pour acheter l'item non present dans la liste et affiche les produits de la shop
		shop.buy(item2);
		
    //met à jour le Cash
        shop.updateCash();
        
    //affiche le Cash
        System.out.println(shop.updateCash());

    }
}