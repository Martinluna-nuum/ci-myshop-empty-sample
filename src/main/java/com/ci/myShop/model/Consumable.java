package com.ci.myShop.model;

public class Consumable extends Item{
	
	private int quantity;

	public Consumable(String name, int id, float price, int nbrElt, int quantity) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
