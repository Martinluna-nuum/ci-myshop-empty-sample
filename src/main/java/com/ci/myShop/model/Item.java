package com.ci.myShop.model;

public class Item {
	//variables de l'obj Item
	String name;
	int id;
	float price;
	int nbrElt;
	
	public Item() {
		super();
		// TODO Auto-generated constructor stub
	}
	//Constructeur du Item
	public Item(String name, int id, float price, int nbrElt) {
		super();
		this.name = name;
		this.id = id;
		this.price = price;
		this.nbrElt = nbrElt;
	}
	//Ensemble de getters et setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getNbrElt() {
		return nbrElt;
	}

	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}
	
	//Fonction to String
	public String toString(){
		String result = "Name:" + name + ", Id:" + id + ", Price:" + price + ", nbrElt:" + nbrElt;  
		
		return result;
	}
}
