package com.ci.myShop.controller;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import com.ci.myShop.model.Item;

public class Storage {
	
	Map<String, Item> itemMap;
	

	//Constructeur du Storage avec une liste d'Items qui vont devenir HashMap
	public Storage(List<Item> items) {
		super();
		itemMap = new HashMap<String, Item>(); 
		for(int i=0;i<items.size();i++)
		{
			Item item = items.get(i);

			addItem(item);
		};
	}
	
	//Recupère les produits du Storage
	public Map<String, Item> getProducts() { 
		
		return itemMap;
		
	}
	
	//Rajoute un Item au Storage
	public void addItem(Item obj) {

		itemMap.put(obj.getName(), obj);
		
	}
	
	//Recupère un produit du Storage
	public Item getItem(String name) {
		
		Item item = itemMap.get(name);
		
		return item;	
	}
	
}
