package com.ci.myShop.controller;

import java.util.List;

import com.ci.myShop.model.Item;

public class Shop {
	//variables
	Storage storage;
	float cash;
	
	//Constructeur du Shop avec une liste 
	public Shop(List<Item> items) {
		super();
		storage = new Storage(items);
		
	}
	
	//Fonction pour vendre un Item
	public Item sell(String name) {
		
		Item item = storage.getItem(name);
		
		cash = cash + item.getPrice();
	
		return item;
		
	}
	
	//Fonction pour mettre à jour le cash
	public float updateCash() {
		
		return cash;
		
	}
	//Fonction pour acheter un produit et afficher une liste mise à jour avec tous les produits
	public boolean buy(Item item1) {
		
		if (cash >= item1.getPrice()) { 
			
			storage.addItem(item1);
			
			storage.getProducts().forEach((name, item) -> {
			System.out.println(name); 
			System.out.println(item);});
			
		
		cash = cash - item1.getPrice();
		
		return true;
			
		}
		else { System.out.println("cash insuffissant"); 
		
		return false;
		}
			
	}
}
