package com.ci.myShop.model;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class ItemTest {
	
	@Test
	public void createEmptyItem() {
		Item it = new Item ();
		assertNotEquals(null, it);
	}

	@Test
	public void updateParameters() {
		Item it = new Item ("name", 100, 5, 10);
		assertEquals("name", it.getName());
		
	}
}

