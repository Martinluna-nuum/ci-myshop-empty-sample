package com.ci.myShop.model;


import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

import com.ci.myShop.controller.Storage;

public class StorageTest {
	
	Item item;
	List<Item> items ;
	
	@Before
	public void init() {
		item = new Item("Zelda", 1, 10, 1);
		items = new ArrayList<Item>();	
	}
	
	@Test
	public void addItemTest() {
		// TODO Auto-generated method stub
		
		Storage storage = new Storage(items);
		storage.addItem(item);
		Item testItem = storage.getItem("Zelda");
		assertEquals(item, testItem);
	}
	
	public void getProducts() {
		
		Storage storage = new Storage(items);
		storage.addItem(item);
		assertEquals(1, storage.getProducts().size());
		
	}
		
	}


